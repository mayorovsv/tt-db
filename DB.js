'use strict';

const sql = require('mssql');
const settings = require('../../settings');

let db;

function connectDatabase() {
  if (!db) {
    db = new sql.Connection(settings.db);
    db.connect().then(() => {
      console.log(`Connected to database ${settings.db.database} at ${settings.db.server}`);
    }).catch(err => {
      console.log(`Error connecting database: ${err}`);
    });
  }
  return db;
}

sql.db = connectDatabase();

module.exports = sql;
